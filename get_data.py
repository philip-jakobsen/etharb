#!/usr/bin/env python3

import requests
import json
import csv
import datetime as dt
import time
import pandas as pd

# unix timestamps
start = 1614553200 # 20210301 2021 march 1st
end = 1614653200

#end = 1631484000 # 20210901 2021 september 1st

pair_name = "ETHEUR"

# general format of ticker_name on Kraken is "X<crypto>Z<fiat>"
ticker_name="XETHZEUR"

def download_batch(start: int) -> tuple[list[list], int]:
    """ 
    Download a batch of trades (which is 1000 trades) from the Kraken API (https://docs.kraken.com/rest/) 
    and return the data plus the timestamp of the last downloaded trade
    """

    response = requests.get(f'https://api.kraken.com/0/public/Trades?pair={pair_name}&since={start}')

    # convert to JSON
    res = response.json()

    batch: list[list] = res['result'][ticker_name]

    # remove useless last empty column
    for i in batch:
        i.pop()

    # set end time to timestamp of last trade in batch
    end: int = batch[-1][2]

    return (batch, end)


def download_all(start: int, end: int) -> list[list]:
    """
    Given start and end, download all data for a given pair as defined in the top
    """

    data = []

    # get first batch end (start of new)
    batch, start = download_batch(start)

    # we use extend() instead of append() as the batch is a list
    data.extend(batch)

    # main while loop
    while start <= end:

        start_dt = dt.datetime.fromtimestamp(start).isoformat()

        print(start_dt)

        batch, batch_end = download_batch(start)

        if start == batch_end:
            # this is the case of multiple trades occuring at the exact time on the last trade of the batch, 
            # so we add a single millisecond
            start = batch_end + 1
        else:
            start = batch_end

        data.extend(batch)

        # wait 3500 ms as not to overload Kraken's API
        time.sleep(3.5)

    return data


def write_data():
    """
    Write the downloaded data to a csv-file
    """

    data = download_all(start, end)
    
    # add header
    header = ['price','size','time','side','type']
    
    # we are lazy and use Pandas to remove duplicate rows.
    df = pd.DataFrame(data, columns = header)
    
    df = df.drop_duplicates()
    
    # reorder columns for convenience
    df = df[['time', 'size', 'price', 'side' ,'type']]
    
    # save to csv (with no indes)
    # also restricting float format to 5 decimals as that is the accuracy of the source
    df.to_csv(f"{pair_name}_uniq_test.csv", index = False, float_format="%.5f")


write_data()





